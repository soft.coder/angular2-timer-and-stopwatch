import {AbstractControl} from '@angular/forms';

export function numericValidator(control: AbstractControl): {[key: string]: any} | null {
  const isValid = isNaN(parseInt(control.value)) || control.value === '';
  return isValid ? {'numericValidator': {value: control.value}} : null;
}
