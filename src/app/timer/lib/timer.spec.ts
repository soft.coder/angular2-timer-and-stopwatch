import {Timer, TimerErrorInvalidState} from './timer';
import {WindowIntervalStub} from '../../common/test/window-interval-stub';
import {CurrentDateTimeStub} from '../../common/adapters/current-date-time-stub';
import {AudioAlarmFactoryStub} from './factories/audio-alarm-factory-stub';
import {AudioAlarmStub} from './adapters/audio-alarm-stub';
import {AudioParametersFactoryStub} from './factories/audio-parameters-factory-stub';

describe('Timer', () => {
  let audioAlarmFactoryStub = new AudioAlarmFactoryStub();
  let audioParametersFactoryStub = new AudioParametersFactoryStub();

  it('should create an instance', () => {
    let name = 'name', totalTime = new Date(1000 * 60 * 10);

    let timer = new Timer(totalTime, name);

    expect(timer).toBeTruthy();
    expect(timer.name).toBe(name);
    expect(timer.totalTime).toBe(totalTime);
  });
  it('should disposable', () => {
    let timer = new Timer(new Date);

    expect(timer.dispose).toBeInstanceOf(Function);
  });
  describe('start', () => {
    let timer: Timer, windowIntervalStub: WindowIntervalStub;
    beforeEach(() => {
      windowIntervalStub = new WindowIntervalStub();
      let audioParams = audioParametersFactoryStub.create('');
      let audioAlarmStub = audioAlarmFactoryStub.create(audioParams);
      timer = new Timer(new Date, '', audioAlarmStub, windowIntervalStub);
    });

    it('should run', () => {
      timer.start();
      expect(windowIntervalStub.countSetCalled).toBe(1);
      expect(timer.isStart).toBeTruthy();
    });
    it('should throw TimerErrorInvalidState in running state', () => {
      timer.start();
      try {
        timer.start()
      }
      catch(error) {
        expect(error).toBeInstanceOf(TimerErrorInvalidState);
        let errorTimer = error as TimerErrorInvalidState;
        expect(errorTimer.isStart).toBeTruthy();
        expect(errorTimer.isPause).toBeFalsy();
        return;
      }
      fail('not throw error');
    });
    it('should throw TimerErrorInvalidState in pause state', () => {
      timer.start();
      try {
        timer.pause();
        timer.start();
      }
      catch(error) {
        expect(error).toBeInstanceOf(TimerErrorInvalidState);
        let errorTimer = error as TimerErrorInvalidState;
        expect(errorTimer.isStart).toBeFalsy();
        expect(errorTimer.isPause).toBeTruthy();
        return;
      }
      fail('not throw error');
    })
  });
  describe('pause', () => {
    let timer: Timer, windowIntervalStub: WindowIntervalStub, audioAlarmStub: AudioAlarmStub;
    beforeEach(() => {
      windowIntervalStub = new WindowIntervalStub();
      let audioParams = audioParametersFactoryStub.create('');
      audioAlarmStub = audioAlarmFactoryStub.create(audioParams);
      timer = new Timer(new Date, '', audioAlarmStub, windowIntervalStub);
    });
    it('should pause', () => {
      timer.start();
      timer.pause();
      expect(windowIntervalStub.countSetCalled).toBe(1);
      expect(windowIntervalStub.countClearCalled).toBe(1);
      expect(audioAlarmStub.countPauseCalled).toBe(1);
      expect(timer.isStart).toBeFalsy();
      expect(timer.isPause).toBeTruthy();
    });

    it('should throw TimerErrorInvalidState in not running state', () => {
      try {
        timer.pause();
      }
      catch(error) {
        expect(error).toBeInstanceOf(TimerErrorInvalidState);
        let errorTimer = error as TimerErrorInvalidState;
        expect(errorTimer.isStart).toBeFalsy();
        expect(errorTimer.isPause).toBeFalsy();
        expect(windowIntervalStub.countClearCalled).toBe(0);
        expect(audioAlarmStub.countPauseCalled).toBe(0);
        return;
      }
      fail('not throw error');
    });
    it('should throw TimerErrorInvalidState in pause state', () => {
      timer.start();
      timer.pause();
      try {
        timer.pause()
      }
      catch(error) {
        expect(error).toBeInstanceOf(TimerErrorInvalidState);
        let errorTimer = error as TimerErrorInvalidState;
        expect(errorTimer.isStart).toBeFalsy();
        expect(errorTimer.isPause).toBeTruthy();
        expect(windowIntervalStub.countClearCalled).toBe(1);
        expect(audioAlarmStub.countPauseCalled).toBe(1);
        return;
      }
      fail('not throw error');
    });
  });
  describe('resume', () => {
    let timer: Timer, windowIntervalStub: WindowIntervalStub, audioAlarmStub: AudioAlarmStub;
    beforeEach(() => {
      windowIntervalStub = new WindowIntervalStub();
      let audioParams = audioParametersFactoryStub.create('');
      audioAlarmStub = audioAlarmFactoryStub.create(audioParams);
      timer = new Timer(new Date, '', audioAlarmStub, windowIntervalStub);
    });
    it('should resume', () => {
      timer.start();
      timer.pause();
      timer.resume();
      expect(windowIntervalStub.countSetCalled).toBe(2);
      expect(windowIntervalStub.countClearCalled).toBe(1);
      expect(timer.isStart).toBeTruthy();
      expect(timer.isPause).toBeFalsy();
    });

    it('should throw TimerErrorInvalidState in running state', () => {
      timer.start();
      try {
        timer.resume();
      }
      catch(error) {
        expect(error).toBeInstanceOf(TimerErrorInvalidState);
        let errorTimer = error as TimerErrorInvalidState;
        expect(errorTimer.isStart).toBeTruthy();
        expect(errorTimer.isPause).toBeFalsy();
        expect(windowIntervalStub.countSetCalled).toBe(1);
        return;
      }
      fail('not throw error');
    });
    it('should throw TimerErrorInvalidState in not running state', () => {
      try {
        timer.resume();
      }
      catch(error) {
        expect(error).toBeInstanceOf(TimerErrorInvalidState);
        let errorTimer = error as TimerErrorInvalidState;
        expect(errorTimer.isStart).toBeFalsy();
        expect(errorTimer.isPause).toBeFalsy();
        expect(windowIntervalStub.countSetCalled).toBe(0);
        return;
      }
      fail('not throw error');
    });

  });
  describe('reset', () => {
    let timer: Timer, windowIntervalStub: WindowIntervalStub, audioAlarmStub: AudioAlarmStub;
    beforeEach(() => {
      windowIntervalStub = new WindowIntervalStub();
      let audioParams = audioParametersFactoryStub.create('');
      audioAlarmStub = audioAlarmFactoryStub.create(audioParams);
      timer = new Timer(new Date, '', audioAlarmStub, windowIntervalStub);
    });
    it('should reset', () => {
      timer.start();
      timer.pause();
      timer.reset();
      expect(windowIntervalStub.countSetCalled).toBe(1);
      expect(windowIntervalStub.countClearCalled).toBe(1);
      expect(audioAlarmStub.countPauseCalled).toBe(1);
      expect(timer.isStart).toBeFalsy();
      expect(timer.isPause).toBeFalsy();
      expect(timer.timeLeft.getTime()).toBe(timer.totalTime.getTime());
      expect(timer.elapsedTime.getTime()).toBe(0);
    });

    it('should throw TimerErrorInvalidState in running state', () => {
      timer.start();
      try {
        timer.reset();
      }
      catch(error) {
        expect(error).toBeInstanceOf(TimerErrorInvalidState);
        let errorTimer = error as TimerErrorInvalidState;
        expect(errorTimer.isStart).toBeTruthy();
        expect(errorTimer.isPause).toBeFalsy();
        expect(windowIntervalStub.countSetCalled).toBe(1);
        expect(windowIntervalStub.countClearCalled).toBe(0);
        expect(audioAlarmStub.countPauseCalled).toBe(0);
        return;
      }
      fail('not throw error');
    });
    it('should throw TimerErrorInvalidState in not running state', () => {
      try {
        timer.reset();
      }
      catch(error) {
        expect(error).toBeInstanceOf(TimerErrorInvalidState);
        let errorTimer = error as TimerErrorInvalidState;
        expect(errorTimer.isStart).toBeFalsy();
        expect(errorTimer.isPause).toBeFalsy();
        expect(windowIntervalStub.countSetCalled).toBe(0);
        expect(windowIntervalStub.countClearCalled).toBe(0);
        expect(audioAlarmStub.countPauseCalled).toBe(0);
        return;
      }
      fail('not throw error');
    });
  });
  describe('window interval handlers', () => {
    let timer: Timer, windowIntervalStub: WindowIntervalStub, audioAlarmStub: AudioAlarmStub;
    let currentDate: CurrentDateTimeStub, timerTotalTime: Date;
    beforeEach(() => {
      timerTotalTime = new Date(100);
      windowIntervalStub = new WindowIntervalStub();
      currentDate = new CurrentDateTimeStub(new Date(1000));
      let audioParams = audioParametersFactoryStub.create('');
      audioAlarmStub = audioAlarmFactoryStub.create(audioParams);
      timer = new Timer(timerTotalTime, '', audioAlarmStub,
        windowIntervalStub, currentDate);
    });

    it('should init timeLeft, elapsedTime, totalTime', () => {

      expect(timer.totalTime.getTime()).toBe(timerTotalTime.getTime());
      expect(timer.timeLeft.getTime()).toBe(timerTotalTime.getTime());
      expect(timer.elapsedTime.getTime()).toBe(0);
      expect(audioAlarmStub.countPlayCalled).toBe(0);
      expect(windowIntervalStub.countSetCalled).toBe(0);
    });

    it('should tick timeLeft and elapsedTime', () => {
      let diffTime = 10;
      let now = new Date(currentDate.get().getTime() + diffTime);

      timer.start();
      currentDate.setCurrentDateTime(now);
      windowIntervalStub.tick();

      expect(timer.isFinished).toBeFalse();
      expect(timer.timeLeft.getTime()).toBe(timerTotalTime.getTime() - diffTime);
      expect(timer.elapsedTime.getTime()).toBe(diffTime);
      expect(audioAlarmStub.countPlayCalled).toBe(0);
      expect(windowIntervalStub.countSetCalled).toBe(1);
      expect(windowIntervalStub.countClearCalled).toBe(0);
    });

    it('should finish', () => {
      let diffTime = 110;
      let now = new Date(currentDate.get().getTime() + diffTime);

      timer.start();
      currentDate.setCurrentDateTime(now);
      windowIntervalStub.tick();

      expect(timer.isFinished).toBeTrue();
      expect(timer.timeLeft.getTime()).toBe(timerTotalTime.getTime() - diffTime);
      expect(timer.elapsedTime.getTime()).toBe(diffTime);
      expect(timer.elapsedTime.getTime()).toBe(diffTime);
      expect(audioAlarmStub.countPlayCalled).toBe(1);
      expect(windowIntervalStub.countSetCalled).toBe(1);
      expect(windowIntervalStub.countClearCalled).toBe(0);
    });
   });

});
