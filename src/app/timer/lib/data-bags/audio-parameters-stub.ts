import {IAudioParameters} from './iaudio-parameters';

export class AudioParametersStub implements IAudioParameters {
  constructor(public audioFileSrc: string,
              public isLoop: boolean,
              public volume: number) {}

}
