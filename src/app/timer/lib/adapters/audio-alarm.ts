import {IAudioAlarm} from './iaudio-alarm';
import {IAudioParameters} from '../data-bags/iaudio-parameters';

export class AudioAlarm implements IAudioAlarm {
  _audio: HTMLAudioElement;
  protected _isPlaying: boolean = false;
  protected _initVolume: number = 1;
  protected _isMuted: boolean = false;


  get isPlaying() : boolean {
    return this._isPlaying;
  }
  get isMuted() : boolean {
    return this._isMuted;
  }

  constructor(params: IAudioParameters) {
    this._audio = new Audio(params.audioFileSrc);
    this._audio.loop = params.isLoop;
    this._audio.volume = this._initVolume = params.volume;
  }

  pause(): void {
    if(this.isPlaying) {
      this._audio.pause();
      this._isPlaying = false;
    }
  }

  play(): Promise<void> {
    let playSound = (): Promise<void> => {
      return this._audio.play().then(() => {
        this._isPlaying = true;
        this._audio.addEventListener('ended', () => {
          this._isPlaying = false;
        });

      });
    };

    if(this._audio.readyState >= 2)
      return playSound();
    else
      return new Promise<void>((resolve => {
        this._audio.addEventListener('canplaythrough', () => {
          resolve(playSound());
        })
      }));
  }

  mute(): void {
    this._audio.volume = 0;
    this._isMuted = true;
  }
  unmute(): void {
    this._audio.volume = this._initVolume;
    this._isMuted = false;
  }

}
