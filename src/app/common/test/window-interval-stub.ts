import {IWindowInterval} from '../adapters/iwindow-interval';

export class WindowIntervalStub implements IWindowInterval {
  countSetCalled = 0;
  countClearCalled = 0;
  countTickCalled = 0;
  callback: Function = () => {};
  clear(id: number): void {
    this.countClearCalled++;
  }

  set(callback: Function, delay: number): number {
    this.countSetCalled++;
    this.callback = callback;
    return 1;
  }
  tick() {
    this.countTickCalled++;
    this.callback();
  }

}
