export class CommonUtils {
  private static addZeroPrefix(value: number): string {
    if(value < 10)
      return '0' + value;
    else
      return value.toString();
  }
  static toTimeString(date: Date, disableTimeZone: boolean = false) {
    let isNegative = false;
    if(date.getTime() < 0) {
      isNegative = true;
      date = new Date(-date.getTime());
    }
    let hours = date.getHours();
    if(disableTimeZone) {
      hours += date.getTimezoneOffset() / 60;
      if(hours < 0)
        hours = 24 + hours;
    }
    let strHours = this.addZeroPrefix(hours);
    let strMinutes = this.addZeroPrefix(date.getMinutes());
    let strSeconds = this.addZeroPrefix(date.getSeconds());
    let strSign = isNegative ? '-' : '';
    return `${strSign}${strHours}:${strMinutes}:${strSeconds}`;
  }
  static toDateString(date: Date, disableTimeZone: boolean = false) {
    var year = date.getFullYear();
    var month = this.addZeroPrefix(date.getMonth() + 1);
    var day = this.addZeroPrefix(date.getDate());
    var timeString = this.toTimeString(date, disableTimeZone);
    return `${day}.${month}.${year} ${timeString}`;
  }
}
