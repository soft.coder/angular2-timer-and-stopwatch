import {Timer} from '../timer';

export interface ITimerService {
  readonly timers: Timer[];
  add(timer: Timer): void;
  remove(timer: Timer): void;
}
