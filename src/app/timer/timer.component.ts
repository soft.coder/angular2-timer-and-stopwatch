import { Component, OnInit } from '@angular/core';
import {Timer} from './lib/timer';
import {TimerService} from './lib/services/timer.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TimerFactory} from './lib/factories/timer-factory';
import {numericValidator} from '../common/validators/numeric.validator';
import {DefaultAudioFiles} from './lib/constants/audio-files';
import {FileAudioInfo} from './lib/data-bags/file-audio-info';
import {AudioAlarmFactory} from './lib/factories/audio-alarm-factory';
import {IAudioAlarm} from './lib/adapters/iaudio-alarm';
import {AudioParametersFactory} from './lib/factories/audio-parameters-factory';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.less']
})
export class TimerComponent implements OnInit {

  timers: Timer[] = [];
  isAddMode: boolean = false;

  formAddTimer: FormGroup = this.fb.group({
    hours: ['', [numericValidator, Validators.min(0), Validators.max(23)]],
    minutes: ['', [numericValidator, Validators.min(0), Validators.max(59)]],
    seconds: ['', [numericValidator, Validators.min(0), Validators.max(59)]],
    name: [''],
    selectedSoundFileSrc: [DefaultAudioFiles[0].src],
    isLoop: [true],
    volume: [100, [Validators.min(0), Validators.max(100)]],
  }) ;

  initialFormValue = this.formAddTimer.value;

  soundFiles: FileAudioInfo[] = DefaultAudioFiles;

  constructor(protected timerService: TimerService,
              protected timerFactory: TimerFactory,
              protected audioAlarmFactory: AudioAlarmFactory,
              protected audioParametersFactory: AudioParametersFactory,
              private fb: FormBuilder) { }

  ngOnInit(): void {
    this.timers = this.timerService.timers;
  }
  addTimer() {
    let {
      hours = 0,
      minutes = 5,
      seconds = 0,
      name = '',
      selectedSoundFileSrc,
      isLoop,
      volume
    } = this.formAddTimer.value;
    hours = parseInt(hours);
    minutes = parseInt(minutes);
    seconds = parseInt(seconds);
    let audioAlarm: IAudioAlarm | null = null;
    if(selectedSoundFileSrc) {
      let audioParameters = this.audioParametersFactory.create(selectedSoundFileSrc,
        isLoop, volume / 100);
      audioAlarm = this.audioAlarmFactory.create(audioParameters);
    }
    let timer = this.timerFactory.createTimer(hours, minutes, seconds, name, audioAlarm);
    this.timerService.add(timer)
  }
  remove(timer: Timer) {
    this.timerService.remove(timer);
  }
  start(timer: Timer) {
    timer.start();
  }
  pause(timer: Timer) {
    timer.pause();
  }
  mute(timer: Timer) {
    timer.mute();
  }
  unmute(timer: Timer) {
    timer.unmute();
  }
  resume(timer: Timer) {
    timer.resume();
  }
  reset(timer: Timer) {
    timer.reset();
  }

  showAddTimerForm() {
    this.isAddMode = true;
  }

  cancelAddTimerForm() {
    this.isAddMode = false;
    this.formAddTimer.reset(this.initialFormValue);
  }

}
