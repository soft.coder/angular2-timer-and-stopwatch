import {IWindowInterval} from '../../../common/adapters/iwindow-interval';
import {Timer} from '../timer';
import {IAudioAlarm} from '../adapters/iaudio-alarm';
import {ICurrentDateTime} from '../../../common/adapters/icurrent-date-time';

export interface ITimerFactory {
  createTimer(hours: number,
              minutes: number,
              seconds: number,
              name: string,
              audioAlarm: IAudioAlarm,
              windowInterval: IWindowInterval,
              currentDateTime: ICurrentDateTime): Timer;
}
