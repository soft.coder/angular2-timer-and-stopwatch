import {IAudioParametersFactory} from './iaudio-parameters-factory';
import {IAudioParameters} from '../data-bags/iaudio-parameters';
import {AudioParametersStub} from '../data-bags/audio-parameters-stub';

export class AudioParametersFactoryStub implements IAudioParametersFactory {
  create(audioFileSource: string, isLoop: boolean = true, volume: number = 1): IAudioParameters {
    return new AudioParametersStub(audioFileSource, isLoop, volume);
  }
}
