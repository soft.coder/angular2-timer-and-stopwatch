import {IAudioAlarm} from './iaudio-alarm';
import {IAudioParameters} from '../data-bags/iaudio-parameters';

export class AudioAlarmStub implements IAudioAlarm {

  isPlaying = false;
  isMuted = false;

  countPauseCalled = 0;
  countPlayCalled = 0;
  countMuteCalled = 0;
  countUnmuteCalled = 0;

  constructor(protected _params: IAudioParameters) { }

  pause(): void {
    this.isPlaying = false;
    this.countPauseCalled++;
  }

  play(): Promise<void> {
    this.isPlaying = true;
    this.countPlayCalled++;
    return new Promise(() => {});
  }
  mute(): void {
    this.isMuted = true;
    this.countMuteCalled++;
  }
  unmute(): void {
    this.isMuted = false;
    this.countUnmuteCalled++;
  }

}
