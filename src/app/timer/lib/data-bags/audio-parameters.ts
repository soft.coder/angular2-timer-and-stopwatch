import {DefaultAudioFiles} from '../constants/audio-files';
import {IAudioParameters} from './iaudio-parameters';

export class AudioParameters implements IAudioParameters{
  get audioFileSrc() {
    return this._audioFileSrc;
  }
  get isLoop() {
    return this._isLoop;
  }
  get volume() {
    return this._volume;
  }
  constructor(protected _audioFileSrc: string = DefaultAudioFiles[0].src,
              protected _isLoop: boolean = true,
              protected _volume: number = 1,)
  {
    if(this.volume < 0 || this.volume > 1) {
      throw new AudioParametersVolumeExceedRangeError(this.volume);
    }
  }
}

export class AudioParametersErrors extends Error {
  constructor(message: string) {
    super(message)
  }
}
export class AudioParametersVolumeExceedRangeError extends AudioParametersErrors {
  get volumeValue() {
    return this._volumeValue;
  }
  constructor(protected _volumeValue: number) {
    super("Volume exceeded range between 0 to 1: " + _volumeValue);
  }
}
