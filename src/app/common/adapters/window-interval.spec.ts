import { WindowInterval } from './window-interval';

describe('WindowInterval', () => {
  it('should create an instance', () => {
    expect(new WindowInterval()).toBeTruthy();
  });
  it('should set interval', (done: DoneFn) => {
    let wi = new WindowInterval();
    wi.set(() => {
      done();
    }, 0);
    expect(wi).toBeDefined();
  });
  it('should clear interval', (done: DoneFn) => {
    let wi = new WindowInterval();
    let id = wi.set(() => {
      done.fail('should clear')
    }, 16);
    wi.clear(id);
    setTimeout(() => {
      done();
    }, 32);
    expect(id > 0).toBeTruthy();
  })

});
