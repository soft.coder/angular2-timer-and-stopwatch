import {IAudioParametersFactory} from './iaudio-parameters-factory';
import {AudioParameters} from '../data-bags/audio-parameters';
import {IAudioParameters} from '../data-bags/iaudio-parameters';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AudioParametersFactory implements IAudioParametersFactory {
  create(audioFileSource: string, isLoop: boolean = true, volume: number = 1): IAudioParameters {
    return new AudioParameters(audioFileSource, isLoop, volume);
  }
}
