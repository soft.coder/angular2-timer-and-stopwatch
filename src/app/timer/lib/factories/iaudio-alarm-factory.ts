import {IAudioAlarm} from '../adapters/iaudio-alarm';
import {IAudioParameters} from '../data-bags/iaudio-parameters';

export interface IAudioAlarmFactory {
  create(params: IAudioParameters): IAudioAlarm;
}
