import { Pipe, PipeTransform } from '@angular/core';
import {Interval} from '../data-bags/interval';
import {CommonUtils} from '../../../common/utils/common-utils';

@Pipe({
  name: 'interval'
})
export class IntervalPipe implements PipeTransform {


  transform(value: Interval, ...args: unknown[]): string {
    let date: Date;
    if(args[0] === 'start' || args[0] === undefined)
      date = value.startDate;
    else
      date = value.endDate;

    if(value.startDate.getDate() === value.endDate.getDate() &&
      value.startDate.getMonth() === value.endDate.getMonth() &&
      value.startDate.getFullYear() === value.endDate.getFullYear()) {
        return CommonUtils.toTimeString(date);
    }
    else {
        return CommonUtils.toDateString(date)
    }
  }

}
