export interface IAudioAlarm {
  isPlaying: boolean;
  isMuted: boolean;
  pause(): void;
  play(): Promise<void>;
  mute(): void;
  unmute(): void;
}
