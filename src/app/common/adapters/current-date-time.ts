import {ICurrentDateTime} from "./icurrent-date-time";
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CurrentDateTime implements ICurrentDateTime {
  get(): Date {
    return new Date();
  }
}
