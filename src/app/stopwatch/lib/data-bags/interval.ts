export class Interval {
  description: string = '';
  constructor(public startDate: Date, public endDate: Date) {
  }
  getDateDiff(): Date {
    return new Date(this.endDate.getTime() - this.startDate.getTime());
  }
}
