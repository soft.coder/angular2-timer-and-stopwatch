import {Pipe, PipeTransform} from '@angular/core';

// noinspection AngularMissingOrInvalidDeclarationInModule
@Pipe({
  name: 'timer'
})
export class TimerStubPipe implements PipeTransform{
  countRun = 0;
  lastVal?: Date;
  transform(value: Date, ...args: unknown[]): string {
    this.countRun++;
    this.lastVal = value;
    return value.toString();
  }

}
