import {Interval} from '../data-bags/interval';


export interface IStopwatchService{
  total: Date;
  last: Date;
  intervals: Array<Interval>;
  isStart: boolean;
  start(): void;
  pause(): void;
  reset(): void;
  split(): void;
}
