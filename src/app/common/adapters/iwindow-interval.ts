export interface IWindowInterval {
  set(callback: Function, delay: number): number;
  clear(id: number): void;
}
