import {ITimerFactory} from './itimer-factory';
import {IWindowInterval} from '../../../common/adapters/iwindow-interval';
import {Timer} from '../timer';
import {WindowIntervalStub} from '../../../common/test/window-interval-stub';
import {ICurrentDateTime} from '../../../common/adapters/icurrent-date-time';
import {CurrentDateTime} from '../../../common/adapters/current-date-time';
import {IAudioAlarm} from '../adapters/iaudio-alarm';


export class TimerFactoryStub implements ITimerFactory{
  createTimer(hours: number,
              minutes: number,
              seconds: number,
              name: string,
              audioAlarm: IAudioAlarm,
              windowInterval: IWindowInterval = new WindowIntervalStub(),
              currentDateTime: ICurrentDateTime = new CurrentDateTime()): Timer {

    let date = new Date(0);
    date.setUTCHours(hours);
    date.setUTCMinutes(minutes);
    date.setUTCSeconds(seconds);

    return new Timer(date, name, audioAlarm, windowInterval, currentDateTime);
  }

}
