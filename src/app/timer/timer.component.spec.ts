import {ComponentFixture, TestBed} from '@angular/core/testing';

import { TimerComponent } from './timer.component';
import {TimerService} from './lib/services/timer.service';
import {TimerStubPipe} from '../common/test/timer-stub.pipe';
import {By} from '@angular/platform-browser';
import {TimerStubService} from './lib/services/timer-stub.service';
import {ReactiveFormsModule} from '@angular/forms';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {TimerFactory} from './lib/factories/timer-factory';
import {TimerFactoryStub} from './lib/factories/timer-factory-stub';
import {MatSelectModule} from '@angular/material/select';
import {MatSliderModule} from '@angular/material/slider';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {HarnessLoader} from '@angular/cdk/testing';
import {TestbedHarnessEnvironment} from '@angular/cdk/testing/testbed';
import {MatButtonHarness} from '@angular/material/button/testing';
import {MatInputHarness} from '@angular/material/input/testing';
import {AudioAlarmFactoryStub} from './lib/factories/audio-alarm-factory-stub';
import {AudioAlarmFactory} from './lib/factories/audio-alarm-factory';
import {MatCheckboxHarness} from '@angular/material/checkbox/testing';
import {MatSliderHarness} from '@angular/material/slider/testing';
import {MatSelectHarness} from '@angular/material/select/testing';
import {DefaultAudioFiles} from './lib/constants/audio-files';
import {AudioParametersFactoryStub} from './lib/factories/audio-parameters-factory-stub';
import {AudioParametersFactory} from './lib/factories/audio-parameters-factory';

describe('TimerComponent', () => {
  let component: TimerComponent;
  let fixture: ComponentFixture<TimerComponent>;
  let loader: HarnessLoader;

  async function addTimer(hours: number, minutes: number, seconds: number, name: string,
                          audioFileName : string | undefined = undefined,
                          volume : number | undefined = undefined,
                          isLoop : boolean | undefined = undefined) {
    let inputHours = await loader.getHarness(MatInputHarness.with({selector: '.t3-form-add input[name=hours]'}));
    let inputMinutes = await loader.getHarness(MatInputHarness.with({selector: '.t3-form-add input[name=minutes]'}));
    let inputSeconds = await loader.getHarness(MatInputHarness.with({selector: '.t3-form-add input[name=seconds]'}));
    let inputName = await loader.getHarness(MatInputHarness.with({selector: '.t3-form-add input[name=name]'}));
    // let btnAdd = fixture.debugElement.query(By.css('.t3-form-add [type=submit]'));
    let form = fixture.debugElement.query(By.css('.t3-form-add form'));

    await inputHours.setValue(hours.toString());
    await inputMinutes.setValue(minutes.toString());
    await inputSeconds.setValue(seconds.toString());
    await inputName.setValue(name.toString());

    if(audioFileName !== undefined) {
      let selectFileSource = await loader.getHarness(MatSelectHarness.with({selector: '.t3-form-add .file-source'}));
      await selectFileSource.open();
      let options = await selectFileSource.getOptions({text: audioFileName});
      await options[0].click();


      if(volume !== undefined) {
        let sliderVolume = await loader.getHarness(MatSliderHarness.with({selector: '.t3-form-add .volume'}));
        await sliderVolume.setValue(volume);
      }

      if(isLoop !== undefined) {
        let checkboxIsLoop = await loader.getHarness(MatCheckboxHarness.with({selector: '.t3-form-add .is-loop'}));
        if (isLoop)
          await checkboxIsLoop.check();
        else
          await checkboxIsLoop.uncheck();
      }
    }

    // btnAdd.nativeElement.dispatchEvent(newEvent('click'));
    // form.nativeElement.dispatchEvent(newEvent('submit'));
    form.triggerEventHandler('submit', null);
    fixture.detectChanges()
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        BrowserAnimationsModule,
        MatToolbarModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatSliderModule,
        MatCheckboxModule
      ],
      declarations: [ TimerComponent, TimerStubPipe ],
      providers: [
        {provide: TimerService, useValue: new TimerStubService()},
        {provide: TimerFactory, useValue: new TimerFactoryStub()},
        {provide: AudioAlarmFactory, useValue: new AudioAlarmFactoryStub()},
        {provide: AudioParametersFactory, useValue: new AudioParametersFactoryStub()}
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TimerComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('add timer form', function() {

    it('should have initially false value of isAddMode component property', () => {
      expect(component.isAddMode).toBeFalse();
    });

    describe('closed state', () => {
      it('should hide add timer form', () => {
        let formAddEl = fixture.debugElement.query(By.css('.t3-form-add'));

        expect(formAddEl).toBeNull('form add timer');
      });

      it('should have button "Add timer"', async() => {
        let btnShowAddFormList =  await loader.getAllHarnesses(MatButtonHarness.with({selector: '.t3-show-add-form'}));

        expect(btnShowAddFormList.length).toBe(1);
      });

      it('should show form after click "Add timer" button', async () => {
        let btnShowAddForm = await loader.getHarness(MatButtonHarness.with({selector: '.t3-show-add-form'}));

        await btnShowAddForm.click();

        let formAddEl = fixture.debugElement.query(By.css('.t3-form-add'));
        expect(formAddEl).toBeTruthy();
      });

      it('should change isAddMode to true after click "Add timer" button', async () => {
        let btnShowAddForm = await loader.getHarness(MatButtonHarness.with({selector: '.t3-show-add-form'}));

        await btnShowAddForm.click();

        expect(component.isAddMode).toBeTruthy();
      });

    });

    describe('open state', () => {
      beforeEach(() => {
        component.isAddMode = true;
        fixture.detectChanges();
      });

      it('should show add timer form when isAddMode true', () => {
        let formAddEl = fixture.debugElement.query(By.css('.t3-form-add'));

        expect(formAddEl).toBeTruthy('form add timer');
      });

      it('should "Add timer" button hide itself after click on it', async () => {
          let btnShowAddFormList = await loader.getAllHarnesses(MatButtonHarness.with({selector: '.t3-show-add-form'}));

          expect(btnShowAddFormList.length).toBe(0);
      });

      it('should close form after click "Cancel" button', async() => {
        let btnCancel = await loader.getHarness(MatButtonHarness.with({selector: '.t3-btn-cancel'}));

        await btnCancel.click();

        let formAddEl = fixture.debugElement.query(By.css('.t3-form-add'));
        expect(formAddEl).toBeNull();
      });

      it('should change isAddMode to false after click "Cancel" button', async() => {
        let btnCancel = await loader.getHarness(MatButtonHarness.with({selector: '.t3-btn-cancel'}));

        await btnCancel.click();

        expect(component.isAddMode).toBeFalse();
      });

      it('should reset formGroup after click "Cancel" button', async () => {
        let btnCancel = await loader.getHarness(MatButtonHarness.with({selector: '.t3-btn-cancel'}));
        let hoursInput = await loader.getHarness(MatInputHarness.with({selector: '.t3-form-add input[name=hours]'}));

        await hoursInput.setValue('10');
        await btnCancel.click();

        expect(component.formAddTimer.pristine).toBeTruthy('pristine');
        expect(component.formAddTimer.untouched).toBeTruthy('untouched');
      });


      describe('add timer', () => {
        let hours = 1, minutes = 23, seconds = 45, name = 'timerName';
        let audioFile = DefaultAudioFiles[1], volume = 50, isLoop = false;

        it('should add timer', async() => {
          let audioAlarmFactoryStub = fixture.debugElement.injector.get(AudioAlarmFactory) as AudioAlarmFactoryStub;
          let timerStubService = fixture.debugElement.injector.get(TimerService) as TimerStubService;

          await addTimer(hours, minutes, seconds, name, audioFile.name, volume, isLoop);

          expect(timerStubService.lastAddedTimer?.totalTime.getUTCHours()).toBe(hours);
          expect(timerStubService.lastAddedTimer?.totalTime.getUTCMinutes()).toBe(minutes);
          expect(timerStubService.lastAddedTimer?.totalTime.getUTCSeconds()).toBe(seconds);
          expect(timerStubService.lastAddedTimer?.name).toBe(name);
          expect(audioAlarmFactoryStub.lastAudioParams?.audioFileSrc).toBe(audioFile.src);
          expect(audioAlarmFactoryStub.lastAudioParams?.volume).toBe(volume / 100);
          expect(audioAlarmFactoryStub.lastAudioParams?.isLoop).toBe(isLoop);


        });

        it('should add timer to timer list', async() => {

          let timerStubService = fixture.debugElement.injector.get(TimerService) as TimerStubService;


          await addTimer(hours, minutes, seconds, name, audioFile.name, volume, isLoop);

          if(timerStubService.lastAddedTimer !== undefined) {
            let addedTimer = timerStubService.lastAddedTimer;
            let timerStubPipe = new TimerStubPipe();
            let txtName = fixture.debugElement.query(By.css('.t3-name-row')).nativeElement.textContent?.trim();
            let txtElapsed = fixture.debugElement.query(By.css('.t3-elapsed-time')).nativeElement.textContent?.trim();
            let txtTimeLeft = fixture.debugElement.query(By.css('.t3-time-left')).nativeElement.textContent?.trim();
            let unMuteButtons = await loader.getAllHarnesses(MatButtonHarness.with({selector: '.t3-btn-mute-timer'}));

            let transformedElapsedTime = timerStubPipe.transform(addedTimer.elapsedTime);
            let transformedTimeLeft = timerStubPipe.transform(addedTimer.timeLeft);
            let transformedTotalTime = timerStubPipe.transform(addedTimer.totalTime);
            expect(txtName).toEqual(name);
            expect(txtElapsed).toEqual(transformedElapsedTime);
            expect(txtTimeLeft).toContain(transformedTimeLeft);
            expect(txtTimeLeft).toContain(transformedTotalTime);
            expect(unMuteButtons.length).toBe(1);
          }
          else
            fail('should add timer to timer stub service');
        });


        describe('timer list actions', () => {
          let hours = 1, minutes = 23, seconds = 45, name = 'timerName';
          let audioFile = DefaultAudioFiles[0];
          let timerActions: TimerActions;
          beforeEach(() => {
            component.isAddMode = true;
            fixture.detectChanges();
            timerActions = new TimerActions(loader);
          });

          async function isShowInitialButtons(withAudioAlarm: boolean) {
            expect(await timerActions.btnStartIsExist()).toBeTrue();
            expect(await timerActions.btnRemoveIsExist()).toBeTrue();


            expect(await timerActions.btnPauseIsExist()).toBeFalse();
            expect(await timerActions.btnResumeIsExist()).toBeFalse();
            expect(await timerActions.btnResetIsExist()).toBeFalse();

            if(withAudioAlarm) {
              expect(await timerActions.btnMuteIsExist()).toBeTrue();
              expect(await timerActions.btnUnmuteIsExist()).toBeFalse();
            }
          }

          it('should show start and remove buttons after add timer without audio alarm', async() => {
            await addTimer(hours, minutes, seconds, name);

            await isShowInitialButtons(false);
          });

          it('should show start remove and mute buttons after add timer with audio alarm', async() => {
            await addTimer(hours, minutes, seconds, name, audioFile.name);

            await isShowInitialButtons(true);
          });


          it('should show pause button after start timer', async() => {
            await addTimer(hours, minutes, seconds, name, audioFile.name);
            let btnStart = await timerActions.btnStart();

            await btnStart.click();

            expect(await timerActions.btnPauseIsExist()).toBeTrue();
            expect(await timerActions.btnMuteIsExist()).toBeTrue();

            expect(await timerActions.btnStartIsExist()).toBeFalse();
            expect(await timerActions.btnRemoveIsExist()).toBeFalse();
            expect(await timerActions.btnResumeIsExist()).toBeFalse();
            expect(await timerActions.btnResetIsExist()).toBeFalse();
            expect(await timerActions.btnUnmuteIsExist()).toBeFalse();

          });

          it('should show resume reset and remove buttons after pause timer', async() => {
            await addTimer(hours, minutes, seconds, name, audioFile.name);
            let btnStart = await timerActions.btnStart();

            await btnStart.click();
            let btnPause = await timerActions.btnPause();
            await btnPause.click();

            expect(await timerActions.btnResumeIsExist()).toBeTrue();
            expect(await timerActions.btnResetIsExist()).toBeTrue();
            expect(await timerActions.btnRemoveIsExist()).toBeTrue();
            expect(await timerActions.btnMuteIsExist()).toBeTrue();

            expect(await timerActions.btnStartIsExist()).toBeFalse();
            expect(await timerActions.btnPauseIsExist()).toBeFalse();
            expect(await timerActions.btnUnmuteIsExist()).toBeFalse();
          });


          it('should show initial state after reset timer', async() => {
            await addTimer(hours, minutes, seconds, name, audioFile.name);
            let btnStart = await timerActions.btnStart();

            await btnStart.click();
            let btnPause = await timerActions.btnPause();
            await btnPause.click();
            let btnReset = await timerActions.btnReset();
            await btnReset.click();

            await isShowInitialButtons(true);

            let timerStubFactory = new TimerFactoryStub();
            let audioAlarmFactory = new AudioAlarmFactoryStub();
            let audioParametersFactoryStub = new AudioParametersFactoryStub();
            let audioParams = audioParametersFactoryStub.create('');
            let audioAlarmStub = audioAlarmFactory.create(audioParams);
            let timerWithInitialState = timerStubFactory.createTimer(hours, minutes, seconds, name, audioAlarmStub);
            let elapsedTime = fixture.debugElement.query(By.css('.t3-elapsed-time')).nativeElement?.textContent?.trim();
            let timeLeft = fixture.debugElement.query(By.css('.t3-time-left')).nativeElement?.textContent?.trim();
            let totalTime = fixture.debugElement.query(By.css('.t3-elapsed-time-row span:last-child')).nativeElement?.textContent?.trim();
            var pipe = new TimerStubPipe();
            expect(totalTime)
              .toBe(pipe.transform(timerWithInitialState.totalTime));
            expect(timeLeft)
              .toBe(pipe.transform(timerWithInitialState.timeLeft));
            expect(elapsedTime)
              .toBe(pipe.transform(timerWithInitialState.elapsedTime));

          });

          it('should remove timer', async() => {
            await addTimer(hours, minutes, seconds, name);

            let btnStart = await timerActions.btnStart();
            await btnStart.click();
            let btnPause = await timerActions.btnPause();
            await btnPause.click();
            let btnRemove = await timerActions.btnRemove();
            await btnRemove.click();

            expect(fixture.debugElement.queryAll(By.css('.t3-timer')).length).toBe(0);
          });

          it('should mute timer', async() => {
            let audioAlarmFactoryStub = fixture.debugElement.injector.get(AudioAlarmFactory) as AudioAlarmFactoryStub;
            await addTimer(hours, minutes, seconds, name, audioFile.name);


            let btnMute = await timerActions.btnMute();
            await btnMute.click();

            expect(audioAlarmFactoryStub.lastCreatedAudioAlarm?.countMuteCalled).toBe(1, 'countMuteCalled');
            expect(audioAlarmFactoryStub.lastCreatedAudioAlarm?.countUnmuteCalled).toBe(0, 'countUnMuteCalled');
            expect(audioAlarmFactoryStub.lastCreatedAudioAlarm?.isMuted).toBeTrue();

            expect(await timerActions.btnMuteIsExist()).toBeFalse();
            expect(await timerActions.btnUnmuteIsExist()).toBeTrue();
          });


          it('should unmute timer', async() => {
            let audioAlarmFactoryStub = fixture.debugElement.injector.get(AudioAlarmFactory) as AudioAlarmFactoryStub;
            await addTimer(hours, minutes, seconds, name, audioFile.name);

            let btnMute = await timerActions.btnMute();
            await btnMute.click();
            let btnUnmute = await timerActions.btnUnmute();
            await btnUnmute.click();

            expect(audioAlarmFactoryStub.lastCreatedAudioAlarm?.countMuteCalled).toBe(1, 'countMuteCalled');
            expect(audioAlarmFactoryStub.lastCreatedAudioAlarm?.countUnmuteCalled).toBe(1, 'countUnmuteCalled');
            expect(audioAlarmFactoryStub.lastCreatedAudioAlarm?.isMuted).toBeFalse();

            expect(await timerActions.btnMuteIsExist()).toBeTrue();
            expect(await timerActions.btnUnmuteIsExist()).toBeFalse();
          });



          class TimerActions {
            static readonly btnStartSelector = '.t3-btn-start-timer';
            static readonly btnPauseSelector = '.t3-btn-pause-timer';
            static readonly btnResumeSelector = '.t3-btn-resume-timer';
            static readonly btnResetSelector = '.t3-btn-reset-timer';
            static readonly btnRemoveSelector = '.t3-btn-remove-timer';
            static readonly btnMuteSelector = '.t3-btn-mute-timer';
            static readonly btnUnmuteSelector = '.t3-btn-unmute-timer';
            async getBtn(selector: string) {
              return await this.harnessLoader.getHarness(MatButtonHarness.with({selector}));
            }
            async btnStart() {
              return await this.getBtn(TimerActions.btnStartSelector);
            }
            async btnPause() {
              return await this.getBtn(TimerActions.btnPauseSelector);
            }
            async btnResume() {
              return await this.getBtn(TimerActions.btnResumeSelector);
            }
            async btnReset() {
              return await this.getBtn(TimerActions.btnResetSelector);
            }
            async btnRemove() {
              return await this.getBtn(TimerActions.btnRemoveSelector);
            }
            async btnMute() {
              return await this.getBtn(TimerActions.btnMuteSelector);
            }
            async btnUnmute() {
              return await this.getBtn(TimerActions.btnUnmuteSelector);
            }
            async isExist(selector: string) {
              let result = await this.harnessLoader.getAllHarnesses(MatButtonHarness.with({selector}));
              return !!result.length;
            }
            async btnStartIsExist() {
              return await this.isExist(TimerActions.btnStartSelector)
            }
            async btnPauseIsExist() {
              return await this.isExist(TimerActions.btnPauseSelector)
            }
            async btnResumeIsExist() {
              return await this.isExist(TimerActions.btnResumeSelector)
            }
            async btnResetIsExist() {
              return await this.isExist(TimerActions.btnResetSelector)
            }
            async btnRemoveIsExist() {
              return await this.isExist(TimerActions.btnRemoveSelector)
            }
            async btnMuteIsExist() {
              return await this.isExist(TimerActions.btnMuteSelector)
            }
            async btnUnmuteIsExist() {
              return await this.isExist(TimerActions.btnUnmuteSelector)
            }

            constructor(protected harnessLoader: HarnessLoader) {

            }
          }

        })
      })
    });
  });
  });
