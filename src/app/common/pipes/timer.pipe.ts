import { Pipe, PipeTransform } from '@angular/core';
import { CommonUtils } from '../utils/common-utils';

@Pipe({
  name: 'timer'
})
export class TimerPipe implements PipeTransform {

  transform(value: Date, ...args: unknown[]): string {
    let disableTimeZone = args[0] === true || args[0] === undefined;
    return CommonUtils.toTimeString(value, disableTimeZone);
  }

}
