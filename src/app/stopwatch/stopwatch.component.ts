import {Component, OnInit} from '@angular/core';
import {StopwatchService} from './lib/services/stopwatch.service';

@Component({
  selector: 'app-stopwatch',
  templateUrl: './stopwatch.component.html',
  styleUrls: ['./stopwatch.component.less']
})
export class StopwatchComponent implements OnInit {

  get isStartStopwatch() {
    return this._stopwatchService.isStart;
  }
  get lastStopwatch() {
    return this._stopwatchService.last
  }
  get totalStopwatch() {
    return this._stopwatchService.total;
  }
  get intervals() {
    return this._stopwatchService.intervals;
  }

  constructor(protected _stopwatchService: StopwatchService) { }

  ngOnInit(): void {
  }
  start() {
      this._stopwatchService.start();
  }
  pause() {
    this._stopwatchService.pause();
  }
  reset() {
    this._stopwatchService.reset();
  }
  split() {
    this._stopwatchService.split();
  }
}
