
export function newEvent(eventName: string, bubbles = false, cancelable = false) {
  const evt = document.createEvent('CustomEvent');  // MUST be 'CustomEvent'
  evt.initCustomEvent(eventName, bubbles, cancelable, null);
  return evt;
}
export function inputText(element: HTMLInputElement | HTMLTextAreaElement, text: string) {
  element.value = text;
  element.dispatchEvent(newEvent('input'));
}
