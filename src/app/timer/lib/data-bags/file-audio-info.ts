export class FileAudioInfo {
  get name(): string {
    return this._name
  }
  get src(): string {
    return this._src;
  }
  /**
   *  Create instance of FileAudioInfo
   * @param _name name displayed in select sound control
   * @param _src path to file name
   */
  constructor(protected _name: string,
              protected _src: string) {

  }
}
