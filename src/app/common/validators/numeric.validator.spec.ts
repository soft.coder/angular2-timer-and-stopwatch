import {numericValidator} from './numeric.validator';
import {AbstractControl} from '@angular/forms';

describe('Numeric validator', () => {
  let abstractControl : jasmine.SpyObj<AbstractControl>;
  beforeEach(() => {
    abstractControl = jasmine.createSpyObj('AbstractControl', ['value']);
  });
  it('should pass validation of string with empty value', () => {
    abstractControl.value.and.returnValue('');
    expect(numericValidator(abstractControl)).toBeDefined();
  });
  it('should pass validation of string with int value', () => {
      abstractControl.value.and.returnValue('12');
      expect(numericValidator(abstractControl)).toBeDefined();
  });
  it('should pass validation of string started with numeric value', () => {
      abstractControl.value.and.returnValue('12fdsk');

      expect(numericValidator(abstractControl)).toBeDefined();
  });
  it('should`not pass validation of string without numeric value', () => {
      abstractControl.value.and.returnValue('fjksd');
      expect(numericValidator(abstractControl)).toBeDefined();
  });
  it('should`not pass validation of string started with not numeric value', () => {
      abstractControl.value.and.returnValue('fjksd123');
      expect(numericValidator(abstractControl)).toBeDefined();
  });
});
