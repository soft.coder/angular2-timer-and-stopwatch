import {IWindowInterval} from '../../../common/adapters/iwindow-interval';
import {WindowInterval} from '../../../common/adapters/window-interval';
import {Timer} from '../timer';
import {ITimerFactory} from './itimer-factory';
import {Injectable} from '@angular/core';
import {ICurrentDateTime} from '../../../common/adapters/icurrent-date-time';
import {CurrentDateTime} from '../../../common/adapters/current-date-time';
import {IAudioAlarm} from '../adapters/iaudio-alarm';

@Injectable({
  providedIn: 'root'
})
export class TimerFactory implements ITimerFactory {
  constructor() {}

  createTimer(hours: number,
              minutes: number,
              seconds: number,
              name: string,
              audioAlarm: IAudioAlarm | null = null,
              windowInterval: IWindowInterval = new WindowInterval(),
              currentDateTime: ICurrentDateTime = new CurrentDateTime()) {

    let date = new Date(0);
    date.setUTCHours(hours);
    date.setUTCMinutes(minutes);
    date.setUTCSeconds(seconds);

    return new Timer(date, name, audioAlarm, windowInterval, currentDateTime);
  }

}

