import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {NotFoundComponent} from './not-found/not-found.component';
import {StopwatchComponent} from './stopwatch/stopwatch.component';
import {TimerComponent} from './timer/timer.component';

const appRoutes = [
  {
    path: 'not-found',
    component: NotFoundComponent
  },
  {path: 'stopwatch', component: StopwatchComponent},
  {path: 'timer', component: TimerComponent},
  {path: '', pathMatch: 'full', redirectTo: 'stopwatch'},
  {path: '**', redirectTo: 'not-found'}
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
