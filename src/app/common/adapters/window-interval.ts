import {IWindowInterval} from './iwindow-interval';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class WindowInterval implements IWindowInterval{
  clear(id: number): void {
    return window.clearInterval(id);
  }

  set(callback: Function, delay: number): number {
    return window.setInterval(callback, delay);
  }

}
