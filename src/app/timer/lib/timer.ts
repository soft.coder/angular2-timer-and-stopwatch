import {Disposable} from '../../common/disposable';
import {IWindowInterval} from '../../common/adapters/iwindow-interval';
import {WindowInterval} from '../../common/adapters/window-interval';
import {ICurrentDateTime} from '../../common/adapters/icurrent-date-time';
import {CurrentDateTime} from '../../common/adapters/current-date-time';
import {IAudioAlarm} from './adapters/iaudio-alarm';

export class Timer implements Disposable{
  protected _isStart = false;
  protected _isPause = false;
  protected _intervalId = 0;
  protected _timeLeft: Date;
  protected _elapsedTime: Date = new Date(0);
  protected _isFinished: boolean = false;

  get isStart() {
    return this._isStart;
  }
  get isPause() {
    return this._isPause
  }
  get isAllowStart() {
    return !this.isStart && !this.isPause;
  }
  get isAllowPause() {
    return this.isStart && !this.isPause;
  }
  get isFinished() {
    return this._isFinished;
  }
  get isHasAudioAlarm() {
    return this._audio !== null;
  }
  get isMuted() {
    return this._audio?.isMuted
  }
  get isAllowResume() {
    return this.isPause && !this.isStart;
  }
  get isAllowReset() {
    return this.isAllowResume;
  }
  get isAllowRemove() {
    return this.isAllowStart || this.isAllowResume;
  }
  get name() {
    return this._name;
  }
  get totalTime() {
    return this._totalTime;
  }
  get timeLeft() {
    return this._timeLeft
  }
  get elapsedTime() {
    return this._elapsedTime;
  }
  constructor(protected _totalTime: Date,
              private _name: string = '',
              protected _audio: IAudioAlarm | null = null,
              protected _windowInterval: IWindowInterval = new WindowInterval(),
              protected _currentDateTime: ICurrentDateTime = new CurrentDateTime())
  {
    this._timeLeft = new Date(this._totalTime.getTime());
  }
  dispose() {
    if(this._intervalId) {
      this._windowInterval.clear(this._intervalId);
      this._audio?.pause();
    }
  }
  start() {
    if(!this.isAllowStart)
      throw new TimerErrorInvalidState('start', this.isStart, this.isPause);

    this._isStart = true;
    this._startInterval();
  }
  pause() {
    if(!this.isAllowPause)
      throw new TimerErrorInvalidState('pause', this.isStart, this.isPause);

    this._isStart = false;
    this._isPause = true;
    this._windowInterval.clear(this._intervalId);
    this._audio?.pause();
  }
  mute() {
    this._audio?.mute();
  }
  unmute() {
    this._audio?.unmute();
  }
  resume() {
    if(!this.isAllowResume)
      throw new TimerErrorInvalidState('resume', this.isStart, this.isPause);

    this._isStart = true;
    this._isPause = false;
    this._startInterval();
  }
  reset() {
    if(!this.isAllowReset)
      throw new TimerErrorInvalidState('reset', this.isStart, this.isPause);
    this._isStart = false;
    this._isPause = false;
    this._timeLeft = new Date(this._totalTime.getTime());
    this._elapsedTime = new Date(0);
    this._isFinished = false;
  }
  protected _startInterval() {
    var prevNow = this._currentDateTime.get();
    this._intervalId = this._windowInterval.set(() => {
      var now = this._currentDateTime.get();
      var diffTime = now.getTime() - prevNow.getTime();
      this._elapsedTime = new Date(this._elapsedTime.getTime() + diffTime);
      this._timeLeft = new Date(this._timeLeft.getTime() - diffTime);
      if(this._timeLeft.getTime() < 0 ) {
        this._isFinished = true;
        if(!this._audio?.isPlaying)
          this._audio?.play();
      }
      prevNow = now;
    }, 985);
  }
}

export class TimerError extends Error {
  constructor(message: string) {
    super(message);
  }
}
export class TimerErrorInvalidState extends TimerError{
  constructor(message: string, public isStart: boolean, public isPause: boolean) {
    super(message);
  }
  toString(): string {
    return `isStart: ${this.isStart}; isPause: ${this.isPause} Message: "${this.message}"`;
  }
}


