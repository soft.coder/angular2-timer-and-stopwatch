
export interface IAudioParameters {
  audioFileSrc: string;
  isLoop: boolean;
  volume: number;
}
