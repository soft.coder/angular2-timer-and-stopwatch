import {IAudioParameters} from '../data-bags/iaudio-parameters';


export interface IAudioParametersFactory {
  create(audioFileSource: string, isLoop: boolean, volume: number): IAudioParameters;
}
