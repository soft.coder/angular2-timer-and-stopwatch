import {IAudioAlarmFactory} from './iaudio-alarm-factory';
import {IAudioAlarm} from '../adapters/iaudio-alarm';
import {AudioAlarm} from '../adapters/audio-alarm';
import {Injectable} from '@angular/core';
import {IAudioParameters} from '../data-bags/iaudio-parameters';

@Injectable({
  providedIn: 'root'
})
export class AudioAlarmFactory implements IAudioAlarmFactory {
  create(audioParams: IAudioParameters): IAudioAlarm {
    return new AudioAlarm(audioParams);
  }

}
