import {Timer} from '../timer';
import {Injectable} from '@angular/core';
import {ITimerService} from './itimer-service';

@Injectable({
  providedIn: 'root'
})
export class TimerStubService implements ITimerService{

  public timers: Timer[] = [];
  public lastAddedTimer?: Timer;
  public lastRemovedTimer?: Timer;
  constructor() { }

  add(timer: Timer): void {
    this.timers.push(timer);
    this.lastAddedTimer = timer;
  }
  remove(timer: Timer): void {
    let index = this.timers.findIndex(t => timer === t);
    this.timers.splice(index, 1);
    this.lastRemovedTimer = timer;
  }
}
