import {FileAudioInfo} from '../data-bags/file-audio-info';

export const DefaultAudioFiles: FileAudioInfo[] = [
  new FileAudioInfo('Alert 1', '/assets/audio/alert1.wav'),
  new FileAudioInfo('Alert 2', '/assets/audio/alert1.wav')
];
