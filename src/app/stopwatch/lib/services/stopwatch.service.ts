import {IStopwatchService} from './istopwatch-service';
import {Interval} from '../data-bags/interval';
import {Injectable, OnDestroy} from '@angular/core';
import {CurrentDateTime} from '../../../common/adapters/current-date-time';
import {WindowInterval} from '../../../common/adapters/window-interval';

@Injectable({
  providedIn: 'root'
})
export class StopwatchService implements IStopwatchService, OnDestroy{
  protected _intervals: Array<Interval> = [];
  protected _isStart: boolean = false;
  protected _last: Date = new Date(0);
  protected _total: Date = new Date(0);

  protected _startDate: Date;
  protected _intervalId: number | undefined;

  get intervals() {
    return this._intervals
  }
  get isStart() {
    return this._isStart;
  }
  get last() {
    return this._last
  }
  get total() {
    return this._total;
  }

  constructor(protected _currentDateTime: CurrentDateTime,
              protected _windowInterval: WindowInterval) {
    this._startDate = this._currentDateTime.get();
  }

  ngOnDestroy(): void {
    if(this._intervalId !== undefined) {
      this._windowInterval.clear(this._intervalId);
      this._intervalId = undefined;
    }
  }

  start(): void {
    this._isStart = true;
    this._startDate = this._currentDateTime.get();
    let prevNow = this._currentDateTime.get();
    this._intervalId = this._windowInterval.set(() => {
      let now = this._currentDateTime.get();
      this._total = new Date( this.total.getTime() + now.getTime() - prevNow.getTime());
      let lastDate: Date | undefined;
      if(this.intervals.length > 0)
        lastDate = this.intervals[this.intervals.length - 1].endDate ;
      else
        lastDate = this._startDate;
      this._last = new Date(now.getTime() - lastDate.getTime());
      prevNow = now;
    }, 1000);
  }

  pause(): void {
    this._isStart = false;
    if(this._intervalId !== undefined) {
      this._windowInterval.clear(this._intervalId);
      this._intervalId = undefined;
    }
  }

  reset(): void {
    this._total = new Date(0);
    this._last = new Date(0);
    this._startDate = new Date(0);
    this.intervals.splice(0, this._intervals.length);
  }

  split(): void {
    let now = this._currentDateTime.get();
    this._last = new Date(0);
    if(this.intervals.length > 0)
      this.intervals.push(
        new Interval(this.intervals[this.intervals.length - 1].endDate, now));
    else
      this.intervals.push(new Interval(this._startDate, now));
  }
}
