import { TestBed } from '@angular/core/testing';

import { TimerService } from './timer.service';
import {Timer} from '../timer';
import {WindowIntervalStub} from '../../../common/test/window-interval-stub';
import {AudioAlarmFactoryStub} from '../factories/audio-alarm-factory-stub';
import {AudioParametersFactoryStub} from '../factories/audio-parameters-factory-stub';

describe('TimerService', () => {
  let service: TimerService;
  let timer: Timer;
  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TimerService);
    timer = new Timer(new Date(0));
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return timers array', () => {
    service.add(timer);

    expect(service.timers).toBeInstanceOf(Array);
    expect(service.timers.length).toBe(1);
    expect(service.timers[0]).toBeInstanceOf(Timer);
  });
  it('should add timer', () => {
    service.add(timer);

    expect(service.countTimers).toBe(1);
    expect(service.timers[0]).toBe(timer);
  });
  it('should return count timers', () => {
    service.add(timer);

    expect(service.countTimers).toBe(1);
  });

  it('should find timer', () => {
    service.add(timer);

    var foundTimer = service.find(timer);

    expect(foundTimer).toBe(timer);
  });
  it('should remove timer', () => {
    let windowIntervalStub = new WindowIntervalStub();
    let audioAlarmFactory = new AudioAlarmFactoryStub();
    let audioParametersFactoryStub = new AudioParametersFactoryStub();
    let audioParams = audioParametersFactoryStub.create('');
    let audioAlarmStub = audioAlarmFactory.create(audioParams);
    let timer2 = new Timer(new Date(0), '', audioAlarmStub, windowIntervalStub);
    service.add(timer);
    service.add(timer2);
    timer2.start();

    service.remove(timer2);

    expect(service.countTimers).toBe(1);
    expect(service.find(timer2)).toBeUndefined();
    expect(windowIntervalStub.countClearCalled).toBe(1);
    expect(audioAlarmStub.isPlaying).toBeFalse();
  });
});
