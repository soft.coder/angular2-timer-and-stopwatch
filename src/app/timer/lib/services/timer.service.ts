import { Injectable } from '@angular/core';
import {Timer} from '../timer';
import {ITimerService} from './itimer-service';

@Injectable({
  providedIn: 'root'
})
export class TimerService implements ITimerService{

  protected _timers: Timer[] = [];
  get timers() {
    return this._timers;
  }
  get countTimers() {
    return this._timers.length;
  }
  constructor() { }

  add(timer: Timer): void {
    this._timers.push(timer);
  }
  remove(timer: Timer): void {
    let index = this._timers.findIndex(t => t === timer);
    this._timers[index].dispose();
    this._timers.splice(index, 1)
  }
  find(timer: Timer): Timer | undefined {
    return this._timers.find(t => timer === t);
  }
}
