import {IAudioAlarmFactory} from './iaudio-alarm-factory';
import {AudioAlarmStub} from '../adapters/audio-alarm-stub';
import {IAudioParameters} from '../data-bags/iaudio-parameters';

export class AudioAlarmFactoryStub implements IAudioAlarmFactory {
  lastCreatedAudioAlarm: AudioAlarmStub | null = null;
  lastAudioParams: IAudioParameters | null = null;
  create(params: IAudioParameters): AudioAlarmStub {
    this.lastAudioParams = params;
    return this.lastCreatedAudioAlarm = new AudioAlarmStub(params);
  }
}
