import {ICurrentDateTime} from './icurrent-date-time';

export class CurrentDateTimeStub implements ICurrentDateTime {
  constructor(protected now: Date) { }
  setCurrentDateTime(currentDateTime: Date) {
    this.now = currentDateTime;
  }
  get() {
    return this.now;
  }
}
